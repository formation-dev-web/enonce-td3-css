TD3 CSS − Unités relatives et design liquide.
==============================================

Exo 1. Fluidification !
-----------------------

Reprenons la maquette du TD2 et rendons la fluide :

- Largeurs / marges et padding gauche/droite en *%*
- Tailles de caractères, padding haut / bas & margin haut / bas en *rem*
- Utiliser *box-sizing: border-box*
- Pour les images des articles, les laisser à leur taille originale fixée en
  pixels
- Pour l'image de fond du header, remplacer l'image par celle fournie dans ce
  dépôt (*header-large.jpg*) ; Selon la largeur du bloc, une plus ou moins
  grande part de l'image de fond sera révélée
- utiliser `min-width` et `max-width` pour limiter le redimensionnement

Astuce :

- *Appliquez la règle de calcul*.

Exo 2. Gérer le boulet…
------------------------

Vous avez utilisé plusieurs choses incompatibles avec IE8 :

- les balises HTML5 ;
- la propriété *border-box* ;
- les unités *rem* ;

Cherchez les mesures (shim, polyfills…), pour que votre site fonctionne
correctement sous IE8 (contrainte du commanditaire du site).

(vous pouvez vous en assurer via le [site de
microsoft](https://developer.microsoft.com/en-us/microsoft-edge/tools/screenshots/)
ou [netrenderer](https://netrenderer.com)).
